package com.a2z.mzone.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import com.a2z.mzone.domain.Product;

@Service
public class ProductService {
	
	private List<Product> products = new ArrayList<>(Arrays.asList(
			new Product("product_1", "lenovo k3 note", "lenovo k3 note description"),
			new Product("product_2", "lenovo k4 note", "lenovo k4 note description"),
			new Product("product_3", "lenovo k5 note", "lenovo k5 note description")
			));
	
	public List<Product> getAllProducts(){
		return products;
	}

	public Product getProduct(String id) {
		return products.stream().filter(p -> p.getId().equals(id)).findFirst().get();
	}

	public boolean addProduct(Product product) {
		products.add(product);
		
		return true;
	}

	public boolean updateProduct(String id, Product product) {
		for(int i=0; i<products.size(); i++){
			Product p = products.get(i);
			
			if(p.getId().equals(id)){
				products.set(i, product);
			}
		}
		
		return true;
	}

	public boolean deleteProduct(String id) {
		products.removeIf(p -> p.getId().equals(id));
		
		return true;
	}
}
