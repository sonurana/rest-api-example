package com.a2z.mzone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.a2z.mzone.domain.*;
import com.a2z.mzone.service.*;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@RequestMapping(method=RequestMethod.GET)
	public List<Product> getAllProducts(){
		return productService.getAllProducts();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Product getProduct(@PathVariable String id){
		return productService.getProduct(id);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public boolean addProduct(@RequestBody Product product){
		return productService.addProduct(product);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public boolean updateProduct(@PathVariable String id, @RequestBody Product product){
		return productService.updateProduct(id, product);
	}
	
	@RequestMapping(value="/{pid}", method=RequestMethod.DELETE)
	public boolean deleteProduct(@PathVariable String pid){
		return productService.deleteProduct(pid);
	}
}
